# FullStackChallange API

## Installation

```bash
$ npm install
```

After installing npm packages create .env file. Fields:
* PORT: project port (default 3000)
* DATABASE: sqlite db name (default fullstack-task)

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Running the tests

```bash
$ npm run test:e2e
```

API Documentation: [Swagger](http://localhost:3000/swagger).