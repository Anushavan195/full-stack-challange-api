import * as request from 'supertest';
import { spawn } from 'child_process';

let serverProcess;
const app = 'http://localhost:3000';

jest.setTimeout(30000);

beforeAll(async () => {
  console.info('Running server...')
  await new Promise((resolve, reject) => {
    serverProcess = spawn('npm', ['start']);

    serverProcess.stdout.on('data', (data) => {
      if (data.includes('Server started at:')) {
        resolve(true)
      }
    });
  })
  console.info('Server started!')
})

afterAll(() => {
  console.log('Killing server...')
  process.kill(serverProcess.pid)
})

describe('BannerController (e2e)', () => {
  it('/banner (GET)', async () => {
    request(app)
      .get('/banner')
      .expect(200)
      .end((err, res) => {
        expect(res.body).not.toBeUndefined()
        expect(res.body).toBeInstanceOf(Array)
      })
  });

  it('/banner (POST)', async () => {
    request(app)
      .post('/banner')
      .send({
        "name": "string",
        "text": "string"
      })
      .expect(200)
      .end((err, res) => {
        expect(res.body.id).not.toBeUndefined()
      })
  });

  it('/banner/:id (GET)', async () => {
    request(app)
      .post('/banner')
      .send({
        "name": "Test name",
        "text": "Test text"
      })
      .end((err, res) => {
        const id = res.body.id;
        request(app)
          .get(`/banner/${id}`)
          .expect(200)
          .end((err, res) => {
            expect(res.body.id).not.toBeUndefined()
            expect(res.body.name).toBe('Test name')
            expect(res.body.text).toBe('Test text')
          })
      })
  });

  it('/banner/:id (PATCH)', async () => {
    request(app)
      .post('/banner')
      .send({
        "name": "Test name",
        "text": "Test text"
      })
      .end((err, res) => {
        const id = res.body.id;
        request(app)
          .patch(`/banner/${id}`)
          .send({
            "name": "Test name changed",
            "text": "Test text changed"
          })
          .expect(200)
          .end((err, res) => {
            expect(res.body.id).not.toBeUndefined()
            expect(res.body.name).toBe('Test name changed')
            expect(res.body.text).toBe('Test text changed')
          })
      })
  });

  it('/banner/:id (DELETE)', async () => {
    request(app)
      .post('/banner')
      .send({
        "name": "Test name",
        "text": "Test text"
      })
      .end((err, res) => {
        const id = res.body.id;
        request(app)
          .delete(`/banner/${id}`)
          .expect(200)
          .end(() => {
            request(app)
              .get(`/banner/${id}`)
              .expect(404)
          })
      })
  });
});
