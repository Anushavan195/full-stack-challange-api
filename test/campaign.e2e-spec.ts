import * as request from 'supertest';
import { spawn } from 'child_process';

let serverProcess;
const app = 'http://localhost:3000';

jest.setTimeout(30000);

let bannerId;

beforeAll(async () => {
  console.info('Running server...')
  await new Promise((resolve, reject) => {
    serverProcess = spawn('npm', ['start']);

    serverProcess.stdout.on('data', (data) => {
      if (data.includes('Server started at:')) {
        resolve(true)
      }
    });
  })
  console.info('Server started!')

  bannerId = await new Promise((resolve, reject) => {
    request(app)
      .post('/banner')
      .send({
        "name": "string",
        "text": "string"
      })
      .expect(200)
      .end((err, res) => {
        resolve(res.body.id)
      })
  })
})

afterAll(() => {
  console.log('Killing server...')
  process.kill(serverProcess.pid)
})

describe('CampaignController (e2e)', () => {
  it('/campaign (GET)', async () => {
    request(app)
      .get('/campaign')
      .expect(200)
      .end((err, res) => {
        expect(res.body).not.toBeUndefined()
        expect(res.body).toBeInstanceOf(Array)
      })
  });

  it('/campaign (POST)', async () => {
    request(app)
      .post('/campaign')
      .send({
        "name": "string",
        "ranges": [
          {
            "start": "11:00",
            "end": "13:00"
          },
          {
            "start": "15:00",
            "end": "16:00"
          }
        ],
        "bannersIds": [
          bannerId
        ]
      })
      .expect(200)
      .end((err, res) => {
        expect(res.body.id).not.toBeUndefined()
      })
  });

  it('/campaign/:id (GET)', async () => {
    request(app)
      .post('/campaign')
      .send({
        "name": "string",
        "ranges": [
          {
            "start": "11:00",
            "end": "13:00"
          },
          {
            "start": "15:00",
            "end": "16:00"
          }
        ],
        "bannersIds": [
          bannerId
        ]
      })
      .end((err, res) => {
        const id = res.body.id;
        request(app)
          .get(`/campaign/${id}`)
          .expect(200)
          .end((err, res) => {
            expect(res.body.id).not.toBeUndefined()
            expect(res.body.name).toBe('string')
          })
      })
  });

  it('/campaign/:id (PATCH)', async () => {
    request(app)
      .post('/campaign')
      .send({
        "name": "string",
        "ranges": [
          {
            "start": "11:00",
            "end": "13:00"
          },
          {
            "start": "15:00",
            "end": "16:00"
          }
        ],
        "bannersIds": [
          bannerId
        ]
      })
      .end((err, res) => {
        const id = res.body.id;
        request(app)
          .patch(`/campaign/${id}`)
          .send({
            "name": "string 2",
            "ranges": [
              {
                "start": "11:30",
                "end": "13:30"
              },
              {
                "start": "15:30",
                "end": "16:30"
              }
            ],
            "bannersIds": []
          })
          .expect(200)
          .end((err, res) => {
            expect(res.body.id).not.toBeUndefined()

            expect(res.body.name).toBe('string 2')

            expect(res.body.activeTimeRanges[0].start).toBe('11:30')
            expect(res.body.activeTimeRanges[0].end).toBe('13:30')

            expect(res.body.activeTimeRanges[1].start).toBe('15:30')
            expect(res.body.activeTimeRanges[1].end).toBe('16:30')

            expect(res.body.banners).toEqual([])
          })
      })
  });

  it('/campaign/:id (DELETE)', async () => {
    request(app)
      .post('/campaign')
      .send({
        "name": "string",
        "ranges": [
          {
            "start": "11:00",
            "end": "13:00"
          },
          {
            "start": "15:00",
            "end": "16:00"
          }
        ],
        "bannersIds": [
          bannerId
        ]
      })
      .end((err, res) => {
        const id = res.body.id;
        request(app)
          .delete(`/campaign/${id}`)
          .expect(200)
          .end(() => {
            request(app)
              .get(`/campaign/${id}`)
              .expect(404)
          })
      })
  });
});