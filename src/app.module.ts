import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { BannerModule } from './presentation/banner/banner.module';
import { CampaignModule } from './presentation/campaign/campaign.module';

require('dotenv').config();

const configService = new ConfigService();

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: configService.get<string>('DATABASE', 'campaigns'),
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    BannerModule,
    CampaignModule
  ],
})
export class AppModule { }
