import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BannerService } from '../banner/banner.service';
import { ActiveTimeRangeDto } from './dto/active-time-range.dto';
import { AssignBannersDto } from './dto/assign-banners.dto';
import { CreateCampaignDto } from './dto/create-campaign.dto';
import { SetActiveTimeRangesDto } from './dto/set-active-time-ranges.dto';
import { UpdateCampaignDto } from './dto/update-campaign.dto';
import { ActiveTimeRangeEntity } from './entities/active-time-range.entity';
import { CampaignEntity } from './entities/campaign.entity';

@Injectable()
export class CampaignService {
  constructor(
    @InjectRepository(CampaignEntity) private campaignRepository: Repository<CampaignEntity>,
    @InjectRepository(ActiveTimeRangeEntity) private activeTimeRangeRepository: Repository<ActiveTimeRangeEntity>,
    private bannerService: BannerService
  ) { }

  async create(createCampaignDto: CreateCampaignDto): Promise<CampaignEntity> {
    const campaignEntity = this.campaignRepository.create(createCampaignDto)

    campaignEntity.activeTimeRanges = []

    for (const timeRange of createCampaignDto.ranges) {
      const timeRangeEntity = await this.activeTimeRangeRepository.save(timeRange)

      campaignEntity.activeTimeRanges.push(timeRangeEntity)
    }

    campaignEntity.banners = []

    for (const bannerId of createCampaignDto.bannersIds) {
      const banner = await this.bannerService.findOne(bannerId)

      campaignEntity.banners.push(banner)
    }

    return await this.campaignRepository.save(campaignEntity)
  }

  async show(): Promise<CampaignEntity[]> {
    let campaigns = await this.campaignRepository.find({ relations: ['banners', 'activeTimeRanges'] });

    campaigns = campaigns.filter(campaign => {
      function dateInRange(date: Date, start: Date, end: Date): boolean {
        const getSeconds = (d) => d.getHours() * 3600 + d.getMinutes();

        if (getSeconds(date) >= getSeconds(start) && getSeconds(date) < getSeconds(end)) return true;

        return false;
      }

      const conditions = []
      const now = new Date()

      campaign.activeTimeRanges.forEach(atr => {
        conditions.push(dateInRange(now, new Date(`0000-01-01 ${atr.start}`), new Date(`0000-01-01 ${atr.end}`)))
      })

      return conditions.includes(true) || campaign.activeTimeRanges.length == 0;
    });

    return campaigns;
  }

  async findAll(): Promise<CampaignEntity[]> {
    const campaigns = await this.campaignRepository.find({ relations: ['banners', 'activeTimeRanges'] });

    return campaigns;
  }

  async findOne(id: number): Promise<CampaignEntity> {
    const entity = await this.campaignRepository.findOne(id, { relations: ['banners', 'activeTimeRanges'] });

    if (!entity) throw new NotFoundException(`Campaign with id: ${id} not found.`);

    return entity;
  }

  async update(id: number, updateBannerDto: UpdateCampaignDto): Promise<CampaignEntity> {
    const campaignEntity = await this.findOne(id);

    campaignEntity.name = updateBannerDto.name;

    if (updateBannerDto.ranges) {
      campaignEntity.activeTimeRanges = []

      for (const timeRange of updateBannerDto.ranges) {
        const timeRangeEntity = await this.activeTimeRangeRepository.save(timeRange)

        campaignEntity.activeTimeRanges.push(timeRangeEntity)
      }
    }

    if (updateBannerDto.bannersIds) {
      campaignEntity.banners = []

      for (const bannerId of updateBannerDto.bannersIds) {
        const banner = await this.bannerService.findOne(bannerId)

        campaignEntity.banners.push(banner)
      }
    }

    return await this.campaignRepository.save(campaignEntity)
  }

  async assignBanner(id: number, dto: AssignBannersDto): Promise<CampaignEntity> {
    const entity = await this.findOne(id);

    for (const bannerId of dto.bannersIds) {
      const banner = await this.bannerService.findOne(bannerId)

      if (!entity.banners.find(b => b.id == bannerId))
        entity.banners.push(banner)
    }

    return await this.campaignRepository.save(entity)
  }

  async unAssignBanner(id: number, dto: AssignBannersDto): Promise<CampaignEntity> {
    const entity = await this.findOne(id);

    for (const bannerId of dto.bannersIds) {
      entity.banners = entity.banners.filter(b => b.id != bannerId)
    }

    return await this.campaignRepository.save(entity)
  }

  async setActiveTimeRanges(id: number, dto: SetActiveTimeRangesDto): Promise<CampaignEntity> {
    const campaignEntity = await this.findOne(id);

    campaignEntity.activeTimeRanges = []

    for (const timeRange of dto.ranges) {
      const timeRangeEntity = await this.activeTimeRangeRepository.save(timeRange)

      campaignEntity.activeTimeRanges.push(timeRangeEntity)
    }

    return await this.campaignRepository.save(campaignEntity)
  }

  async remove(id: number): Promise<void> {
    const entity = await this.findOne(id);

    await this.activeTimeRangeRepository.remove(entity.activeTimeRanges)

    await this.campaignRepository.remove(entity)
  }
}
