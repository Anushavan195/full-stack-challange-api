import { Module } from '@nestjs/common';
import { CampaignService } from './campaign.service';
import { CampaignController } from './campaign.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CampaignEntity } from './entities/campaign.entity';
import { BannerModule } from '../banner/banner.module';
import { ActiveTimeRangeEntity } from './entities/active-time-range.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CampaignEntity, ActiveTimeRangeEntity]), BannerModule],
  controllers: [CampaignController],
  providers: [CampaignService]
})
export class CampaignModule {}
