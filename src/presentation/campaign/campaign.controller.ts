import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { SuccessResponse } from '../../types/success-response.model';
import { CampaignService } from './campaign.service';
import { AssignBannersDto } from './dto/assign-banners.dto';
import { CreateCampaignDto } from './dto/create-campaign.dto';
import { SetActiveTimeRangesDto } from './dto/set-active-time-ranges.dto';
import { UpdateCampaignDto } from './dto/update-campaign.dto';
import { CampaignEntity } from './entities/campaign.entity';

@ApiTags('Campaign')
@Controller('campaign')
export class CampaignController {
  constructor(private readonly campaignService: CampaignService) {}

  @Post()
  async create(@Body() createCampaignDto: CreateCampaignDto): Promise<CampaignEntity> {
    return await this.campaignService.create(createCampaignDto);
  }

  @Get()
  async show(): Promise<CampaignEntity[]> {
    return await this.campaignService.show();
  }

  @Get('list/all')
  async findAll(): Promise<CampaignEntity[]> {
    return await this.campaignService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<CampaignEntity> {
    return await this.campaignService.findOne(+id);
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateCampaignDto: UpdateCampaignDto): Promise<CampaignEntity> {
    return await this.campaignService.update(+id, updateCampaignDto);
  }

  // @Patch(':id/assign-banners')
  // async assignBanner(@Param('id') id: string, @Body() dto: AssignBannersDto): Promise<CampaignEntity> {
  //   return await this.campaignService.assignBanner(+id, dto);
  // }

  // @Patch(':id/un-assign-banners')
  // async unAssignBanner(@Param('id') id: string, @Body() dto: AssignBannersDto): Promise<CampaignEntity> {
  //   return await this.campaignService.unAssignBanner(+id, dto);
  // }

  // @Patch(':id/set-active-time-ranges')
  // async setActiveTimeRanges(@Param('id') id: string, @Body() dto: SetActiveTimeRangesDto): Promise<CampaignEntity> {
  //   return await this.campaignService.setActiveTimeRanges(+id, dto);
  // }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<SuccessResponse> {
    await this.campaignService.remove(+id);

    return {
      success: true,
      message: `Banner with id: ${id} successfully removed.`
    }
  }
}
