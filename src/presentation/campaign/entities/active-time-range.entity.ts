import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CampaignEntity } from './campaign.entity';

@Entity('active_time_ranges')
export class ActiveTimeRangeEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "time" })
    start: Date;

    @Column({ type: "time" })
    end: Date;

    @ManyToOne(() => CampaignEntity)
    campaign: CampaignEntity
}
