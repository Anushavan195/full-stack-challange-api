import { BannerEntity } from '../../../presentation/banner/entities/banner.entity';
import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ActiveTimeRangeEntity } from './active-time-range.entity';

@Entity('campaign')
export class CampaignEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToMany(() => BannerEntity, { cascade: ['insert', 'update'] })
    @JoinTable({ name: 'banner_campaigns' })
    banners: BannerEntity[]

    @OneToMany(() => ActiveTimeRangeEntity, atr => atr.campaign, { cascade: true, onDelete: 'CASCADE' })
    activeTimeRanges: ActiveTimeRangeEntity[]
}
