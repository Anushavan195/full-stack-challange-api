import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, Matches } from "class-validator";

export class ActiveTimeRangeDto {
    @Matches(new RegExp('^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$'))
    @IsNotEmpty()
    @ApiProperty()
    start: string;

    @Matches(new RegExp('^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$'))
    @IsNotEmpty()
    @ApiProperty()
    end: string;

    constructor(start = "12:00", end = "15:00") {
        this.start = start;
        this.end = end;
    }
}
