import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsArray, IsNotEmpty, ValidateNested } from "class-validator";
import { ActiveTimeRangeDto } from "./active-time-range.dto";

export class SetActiveTimeRangesDto {
    @IsArray()
    @IsNotEmpty()
    @ValidateNested({ each: true })
    @Type(() => ActiveTimeRangeDto)
    @ApiProperty({ example: [new ActiveTimeRangeDto("11:00", "13:00"), new ActiveTimeRangeDto("15:00", "16:00")] })
    ranges: ActiveTimeRangeDto[]
}
