import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsNotEmpty, IsNumber, IsString } from "class-validator";

export class AssignBannersDto {
    @IsArray()
    @IsNotEmpty()
    @ApiProperty()
    bannersIds: number[];
}
