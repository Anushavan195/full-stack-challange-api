import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateBannerDto } from './dto/create-banner.dto';
import { UpdateBannerDto } from './dto/update-banner.dto';
import { BannerEntity } from './entities/banner.entity';

@Injectable()
export class BannerService {
  constructor(@InjectRepository(BannerEntity) private bannerRepository: Repository<BannerEntity>) { }

  async create(createBannerDto: CreateBannerDto): Promise<BannerEntity> {
    return await this.bannerRepository.save(createBannerDto)
  }

  async findAll(): Promise<BannerEntity[]> {
    return await this.bannerRepository.find();
  }

  async findOne(id: number): Promise<BannerEntity> {
    const entity = await this.bannerRepository.findOne(id, { relations: ['campaigns'] });

    if (!entity) throw new NotFoundException(`Banner with id: ${id} not found.`);

    return entity;
  }

  async update(id: number, updateBannerDto: UpdateBannerDto): Promise<BannerEntity> {
    const entity = await this.findOne(id);

    entity.name = updateBannerDto.name;
    entity.text = updateBannerDto.text;

    return await this.bannerRepository.save(entity)
  }

  async remove(id: number): Promise<void> {
    const entity = await this.findOne(id);

    await this.bannerRepository.remove(entity)
  }
}
