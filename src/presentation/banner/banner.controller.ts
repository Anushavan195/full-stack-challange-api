import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { SuccessResponse } from 'src/types/success-response.model';
import { BannerService } from './banner.service';
import { CreateBannerDto } from './dto/create-banner.dto';
import { UpdateBannerDto } from './dto/update-banner.dto';
import { BannerEntity } from './entities/banner.entity';


@ApiTags('Banner')
@Controller('banner')
export class BannerController {
  constructor(private readonly bannerService: BannerService) {}

  @Post()
  async create(@Body() createBannerDto: CreateBannerDto): Promise<BannerEntity> {
    return await this.bannerService.create(createBannerDto);
  }

  @Get()
  async findAll(): Promise<BannerEntity[]> {
    return await this.bannerService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<BannerEntity> {
    return await this.bannerService.findOne(+id);
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateBannerDto: UpdateBannerDto): Promise<BannerEntity> {
    return await this.bannerService.update(+id, updateBannerDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<SuccessResponse> {
    await this.bannerService.remove(+id);

    return {
      success: true,
      message: `Banner with id: ${id} successfully removed.`
    }
  }
}
