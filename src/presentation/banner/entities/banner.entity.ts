import { CampaignEntity } from '../../../presentation/campaign/entities/campaign.entity';
import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('banners')
export class BannerEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    text: string;

    @ManyToMany(() => CampaignEntity, { cascade: ['insert', 'update'] })
    @JoinTable({ name: 'banner_campaigns' })
    campaigns: CampaignEntity[]
}
