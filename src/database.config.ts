import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

require('dotenv').config();

const configService = new ConfigService();

export const dbConfig: TypeOrmModuleOptions = {
    type: 'sqlite',
    database: configService.get<string>('DATABASE', 'campaigns'),
    entities: ['dist/**/*.entity{.ts,.js}'],
    synchronize: true,
}